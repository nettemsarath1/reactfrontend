
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch, Redirect,withRouter } from "react-router-dom";
import { AvantariContext } from './Context'

import ChatBot from "./components/ChatBot"
import Rating from "./components/Rating"
import DashBoard from "./pages/DashBoard"
import Login from "./pages/Login"
import SignUp from "./pages/SignUp"
import UploadedList from "./components/UploadedFiles"
import NoMatchRoute from './pages/NoMatch'

function App(){
  const value={
    isAuthenticated : localStorage.getItem("Token"),
    displayName: localStorage.getItem("displayName"),
    rating : 1
  }
  return (
    <>
    <AvantariContext.Provider value={value}>

    <BrowserRouter>
    <section>
      <Switch>

      <Route 
        exact path="/" render={ ()=>
          value.isAuthenticated ? (<ChatBot />) : (<Redirect to="/auth" /> )
      } />

      <Route exact path="/auth" component={DashBoard} />
      <Route exact path="/uploadlist" component={ UploadedList } />
      <Route exact path="/rating" component={ Rating } />
      {/* <Route exact path="/foodItem" render={ ()=> true ? (<FoodItem /> ): <Contact /> } /> */}

      <Route path="*" component={NoMatchRoute} />
      </Switch>
      </section>
    </BrowserRouter>

    </AvantariContext.Provider>
    </>
  )
}

export default App ;