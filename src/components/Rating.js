
import React,{ useState, useEffect, useContext } from 'react';
import { useHistory } from "react-router-dom";
import { Button } from "react-bootstrap"
import axios from "axios"
import { AvantariContext } from "../Context"
import StarRatingComponent from 'react-star-rating-component';

function Rating(){
    const History = useHistory();
    const value = useContext(AvantariContext);
    const URL = 'https://mysterious-sands-47664.herokuapp.com/' 

    const [rating,setRating] = useState(value.rating)
    const [status,setStatus]= useState({
        success : false ,
        message : ""
    })

    const onStarClick =(nextValue, prevValue)=>{
        console.log( nextValue,prevValue )
        setRating(nextValue)
    }
    const UserRating =()=>{
        let email = localStorage.getItem('email')
        let config={
            headers:{
                authorization : `Bearer ${ localStorage.getItem("Token") }`
            }
        }
        axios.get(`${URL}rating/${email}`, config ).then(result=>{
            if( result.data.success ){
                setRating( result.data.userRating.rating )
                value.rating = result.data.userRating.rating
            }
        }).catch(error=>{

        })
    }
    const GoBack = ()=>{
        History.goBack();
    }
    useEffect(()=>{
        UserRating()
    },[])
    const SubmitRating =()=>{
        let ratingDetails = {
            rating: rating ,
            email : localStorage.getItem("email")
        }
        let config={
            headers:{
                authorization : `Bearer ${ localStorage.getItem("Token") }`
            }
        }
        axios.post(`${URL}rating`, ratingDetails, config ).then(result=>{
            if( result.data.success ){
               // alert( result.data.message )
                value.rating = rating
                setStatus({
                    success : true,
                    message:result.data.message
                })
            }else{
                setStatus({
                    success : true,
                    message:result.data.message
                })
            }
        }).catch(error=>{
            alert( error.message )
        })
    }

    return (                
        <div className="container" style={{ padding:"100px" }} >
          <Button onClick={ GoBack } >Go Back</Button><br /><br />
          <h2>Please Give The Rating for ChatBot: {rating}</h2>
         
          <StarRatingComponent 
            name="rate1" 
            starCount={10}
            value={rating}
            onStarClick={onStarClick}
          /><br /><br />
        <Button onClick={ SubmitRating } >Submit</Button><br /><br />

        { status.success ? <p> { status.message } </p> : null }
        </div>
    )
}

export default Rating ;
