

import React, { useState,useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Button } from "react-bootstrap"
import axios from "axios"

function UploadedList() {
    const History = useHistory()
    const URL = 'https://mysterious-sands-47664.herokuapp.com/'
    const [uploadedlist, setUploadedlist] = useState({
        files: []
    })
    const filesList=()=>{
        console.log('executing Uploadfiles component')
        let email = localStorage.getItem('email')
        let config={
            headers:{
                authorization : `Bearer ${ localStorage.getItem("Token") }`
            }
        }
        axios.get(`${URL}upload/${email}`,config ).then(result=>{
            console.log('Uploaded files list is:', result )
            if( result.data.success ){
                setUploadedlist( {files:result.data.UploadedFiles} )
            }
        }).catch(error=>{

        })
    }
    useEffect(()=>{
        filesList()
    },[])
    const handleGoBack = ()=>{
       
        History.goBack();
    }
    const DisplayFilesJSX = ()=> {
        return <div className="row" >
        
            { uploadedlist.files.map( file =>{
              return(
                <div className="col-md-4" >
                    <div >

                    FileName: <b>{ file.originalname }</b> <br /><br />
                    
                    Location: <p>{ file.location } </p>
                    Download : 
                    <a href={ file.location }>Download</a>
                    </div>
                </div>
              )
            }) }
    
        </div>
    }

    return (
        <div style={{ margin:"40px" }} >
           
            <Button onClick={ handleGoBack } > Go Back </Button><br />
            <div class="container" style={{ padding:"100px" }} >

            <div style={{ textAlign:"center" }} ><b> Uploaded Files List </b> </div><br />

            { uploadedlist.files.length !==0 ? DisplayFilesJSX(): null }

            </div>
        </div>
    )

}

export default UploadedList ;
