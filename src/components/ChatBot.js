
import React, { useState, useContext } from 'react';
import { useLocation,useHistory,browserHistory } from "react-router";
import { Button } from "react-bootstrap"
import { AvantariContext } from "../Context"
import axios from "axios"
import { auth } from "../fireService"

function ChatBot() {
    const value = useContext(AvantariContext);
    const URL = 'https://mysterious-sands-47664.herokuapp.com/'
    const [chatmessage, setChatMessage] = useState({
        message : '',
        file : '',
        type : ''
    })

    const [messages, setMessages] = useState([{ message:"Iam Siri", from: "client" }])
    const History = useHistory()
    const HandleLogOut = ()=>{
        value.isAuthenticated = false
        localStorage.removeItem("email")
        localStorage.removeItem("Token")
        History.push("/")

        auth().signOut()
            .then(()=> {
                console.log('FireBase Logout success')
            }).catch(error=> {
                console.log('FireBase Logout Error')
        });
    }
    const HandleRating = ()=>{
        History.push("/rating")
    }
    const HandleMessage = (e)=>{
        setChatMessage({ ...chatmessage ,
            [e.target.name] : e.target.value  ,
            type : "message"
        })   
    }

    const HandlefileUpload = (e)=>{
        console.log( e.target.files )
        setChatMessage({ ...chatmessage,
            file : e.target.files[0],
            type: "file"
        })
    }
    const UploadedFilesList = ()=>{
        History.push('/uploadlist')
    }

    const SubmitMessage =()=>{
      if( chatmessage.type=="message" ){
            let newMessages = [...messages] ;
            newMessages.push( { message:chatmessage.message,from:"client" } )
            console.log('cilent messages:', newMessages )
            
            setChatMessage({
                message : '',
                file : '',
                type : ''
            })
            let config={
                headers:{
                    authorization : `Bearer ${ localStorage.getItem("Token") }`
                }
            }
            axios.get(`${URL}message`,config).then(result=>{

                if( result.data.success ){
                    newMessages.push( { message: result.data.message ,from:"server" } )
                    console.log( 'server messages:', newMessages )
                    setMessages( newMessages )
                }

            }).catch(error=>{
                newMessages.push( { message: "Iam sorry...",from:"server" } )
                console.log( 'server messages:', newMessages )
                setMessages( newMessages )
            })
      }else if(chatmessage.type=="file") {
        let formData = new FormData();
        console.log('file uploading api:',  chatmessage.file )
        formData.append('avatar', chatmessage.file );
        formData.append('email', localStorage.getItem("email") )
        const config = {     
            headers: {
                    'content-type': 'multipart/form-data',
                    "authorization" : `Bearer ${ localStorage.getItem("Token") }`
                }
        }
        let newMessages = [...messages] ;
        axios.post( `${URL}upload`, formData ,config).then(result=>{
            console.log('Upload Axios success....')
            if( result.data.success ){
               
                newMessages.push( { message: result.data.message ,from:"server" } )
                console.log( 'server messages:', newMessages )
                setMessages( newMessages )
            }

        }).catch(error=>{
            newMessages.push( { message: "Iam sorry...",from:"server" } )
            console.log( 'server messages:', newMessages )
            setMessages( newMessages )
        })
      }

    }

    const CancelDetails = ()=>{
        console.log( chatmessage.message )
        setChatMessage({ ...chatmessage,
            message : '',
            file : '',
            type : ''
        })
    }
    const messageListJSX = ()=>{
        return  messages.map( (chat,index)=>{
            if( chat.from == "client" ){
                return <div key={ index } style={{ padding: "8px" ,margin:"8px" }} > {chat.message} </div>
            }else{
                return <div key={ index } style={{ float:"right", marginRight:"200px" }} >{ chat.message} </div>
            }
        } ) 
    }

    return <div className="container" style={{ padding:"20px" }} >
        <Button onClick={ HandleRating } variant="warning" style={{ float:"right", margin:"20px" }} > Rating </Button>
        <Button onClick={ HandleLogOut } variant="danger" style={{ float:"right", margin:"20px" }} > LogOut </Button>
        <Button onClick={ UploadedFilesList } variant="primary" style={{ float:"right", margin:"20px" }} >Uploaded Files</Button>
        <br />
        <div style={{ marginLeft:"200px", marginRight:"100px", marginTop:"50px" }} >
            <div style={{ textAlign:"left" }} >
                <b> Chat Bot </b>
            </div>
            <br />

            <div> HII </div>
            { value.displayName }
            { messageListJSX() }
           
            <br /><br />
            <div >
                <input type="text" placeholder="Enter Text" onChange={ HandleMessage } name="message" value={ chatmessage.message } />
                Upload File: <input type="file"  accept="application/pdf" onChange={ HandlefileUpload } /><br /><br />
                <Button  variant="primary" style={{ margin:"5px" }} onClick={ SubmitMessage } > { chatmessage.type==="file" ? "Upload":"Send" } </Button>
                <Button  variant="primary" style={{ margin:"5px" }} onClick={ CancelDetails } > Cancel </Button>
            </div>
        </div>

        
    </div>
}

export default ChatBot ;
