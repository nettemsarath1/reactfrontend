
import React from 'react';
import { Tab, Tabs } from "react-bootstrap"
import Login from "./Login"
import SignUp from "./SignUp"

import 'bootstrap/dist/css/bootstrap.min.css';

function DashBoard(){
   
    return (
        <div className="container" style={{ padding:"40px" }} >
        <Tabs defaultActiveKey="login" transition={false} >
            <Tab eventKey="login" defaultActiveKey title="Login">
                <Login />
            </Tab>
            <Tab eventKey="signUp" title="SignUp">
                <SignUp />
            </Tab>

        </Tabs>
        </div>
    )
}

export default DashBoard 