
 
import React,{ useState } from 'react';
import { Button } from "react-bootstrap";
import Spinner from 'react-bootstrap/Spinner';
import axios from "axios"

function SignUp(){
    const URL = 'https://mysterious-sands-47664.herokuapp.com/'
    const [signUpdetails, setSignUpdetails]= useState({
        firstName:"",
        lastName:"",
        email : "",
        password: "",
        phoneNumber: "",
        message :""
    })
    const [spinner, setSpinner] = useState(false)
    const HandleSignUp = ()=>{
        let userDetails = {
            firstName: signUpdetails.firstName ,
            lastName: signUpdetails.lastName ,
            email : signUpdetails.email ,
            password: signUpdetails.password ,
            phoneNumber: signUpdetails.phoneNumber
        }
        console.log( userDetails )
        setSpinner(true)
        axios.post( `${URL}signUp`, userDetails).then(result=>{
            console.log('Signup Result is :', result.data )
            setSignUpdetails({
                firstName:"",
                lastName:"",
                email : "",                    
                password: "",
                phoneNumber: "",
                message : result.data.message
            })
            setSpinner(false)
        }).catch(error=>{
            setSignUpdetails({
                firstName:"",
                lastName:"",
                email : "",                    
                password: "",
                phoneNumber: "",
                message : error.message
            })
        })
    }
    const handleChange =(e)=>{
        setSignUpdetails({...signUpdetails ,
            [e.target.name]: e.target.value
        })
       
    }
    return (
        <div className="container" style={{ padding:"50px" }}>
       
        <input type="text" name="firstName" placeholder='firstName' value={ signUpdetails.firstName } onChange={ handleChange } /><br /><br />
        <input type="text" name="lastName" placeholder='lastName' value={ signUpdetails.lastName } onChange={ handleChange } /><br /><br />
        <input type="text" name="email" placeholder='Enter Email' value={ signUpdetails.email } onChange={ handleChange } /><br /><br />
        <input type="password" name="password" placeholder='Enter Password' value={ signUpdetails.password } onChange={ handleChange } /><br /><br />
        <input type="text" name="phoneNumber" placeholder='Phone:+919502679717' value={ signUpdetails.phoneNumber } onChange={ handleChange } /><br /><br />

        <Button onClick={ HandleSignUp } > SignUp </Button><br /><br />
        <div style={{ padding:"30px" }} >{ signUpdetails.message }</div>
        { spinner ? <Spinner animation="border" variant="info" /> : null }
      
    </div>        
    )

}

export default SignUp 