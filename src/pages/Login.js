

import React,{ useState, useContext } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { useHistory } from "react-router-dom";
import { loginUser } from '../auth'
import { AvantariContext } from "../Context"
import { auth } from "../fireService"
import { Button } from "react-bootstrap"

function Login(){
    const value = useContext(AvantariContext);
    const History = useHistory()
    const [loginDetails, setLoginDetails] = useState({
        email : "" ,
        password: ""
    })
    const [spinner, setSpinner] = useState(false)
    const [errormessage,setErrorMessage] = useState('')

    const HandleLogin = async()=>{    
        try {
            setSpinner(true)
            let loginResult = await loginUser( loginDetails.email, loginDetails.password )
            console.log("login Result is:", loginResult )
            console.log("email", loginResult.user.email)
            value.isAuthenticated = true
            localStorage.setItem('email',loginResult.user.email)
            localStorage.setItem("displayName", loginResult.user.displayName )
            value.displayName = loginResult.user.displayName
        auth().onAuthStateChanged(function(user) {
            if (user) {
              setSpinner(true)
              user.getIdToken().then(function(data) {
                console.log('Token is>>>>>>>>',data)
                localStorage.setItem("Token",data )
                History.push('/')
              });
            }
          })

        } catch (error) {
            console.log( 'man Error...', error.message )
            setErrorMessage(error.message)
        }

    }
    const handleChange = (e)=>{
        setLoginDetails({ ...loginDetails ,
            [e.target.name]: e.target.value
        })
    }
    return (
        <div className="container" style={{ padding:"50px" }}>
           
            <input type="text" name="email" placeholder='Enter Email' value={ loginDetails.email } onChange={ handleChange } /><br /><br />
            <input type="password" name="password" placeholder='Enter Password' value={ loginDetails.password } onChange={ handleChange } /><br /><br />
        <Button onClick={ HandleLogin } > Login </Button><br /><br />

        <div style={{ padding:"30px" }} >{ errormessage }</div>
        { spinner ? <Spinner animation="border" variant="info" /> : null }
        </div>
    )
}

export default Login ; 